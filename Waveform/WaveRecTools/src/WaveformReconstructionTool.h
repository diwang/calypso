/*
   Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/** @file WaveformReconstructionTool.h
 *  Header file for WaveformReconstructionTool.h
 *
 */
#ifndef WAVERECTOOLS_WAVEFORMRECONSTRUCTIONTOOL_H
#define WAVERECTOOLS_WAVEFORMRECONSTRUCTIONTOOL_H

//Athena
#include "AthenaBaseComps/AthAlgTool.h"
#include "WaveRecTools/IWaveformReconstructionTool.h"

#include "WaveRawEvent/RawWaveform.h"

#include "WaveformBaselineData.h"
#include "WaveformFitResult.h"

//Gaudi
#include "GaudiKernel/ToolHandle.h"

//STL
#include <string>
#include <vector>

class WaveformReconstructionTool: public extends<AthAlgTool, IWaveformReconstructionTool> {
 public:

  /// Normal constructor for an AlgTool; 'properties' are also declared here
  WaveformReconstructionTool(const std::string& type, 
			     const std::string& name, const IInterface* parent);

  /// Retrieve the necessary services in initialize
  StatusCode initialize();

  /// Reconstruct hits from waveform
  
  virtual StatusCode reconstruct(const RawWaveform& wave,
				 const xAOD::WaveformClock* clock,
				 xAOD::WaveformHitContainer* container) const;

 private:

  //
  // Baseline Estimation Parameters
  BooleanProperty m_useSimpleBaseline{this, "UseSimpleBaseline", false};
  IntegerProperty m_samplesForBaselineAverage{this, "SamplesForBaselineAverage", 40};

  //
  // Parameters of initial histogram to find baseline max
  // Range and bins to use, ratio should be an integer (bin width)
  IntegerProperty m_baselineRange{this, "BaselineRange", 16000};
  IntegerProperty m_baselineRangeBins{this, "BaselineRangeBins", 320};

  // 
  // Parameters for the Gaussian fit to the baseline peak (in counts)
  // Range is total range to use to find truncated mean and width
  IntegerProperty m_baselineFitRange{this, "BaselineFitRange", 200};
  // Fit window is value (in sigma) of trucated width to use in final fit
  FloatProperty m_baselineFitWindow{this, "BaselineFitWindow", 2.};

  //
  // Peak threshold (in sigma of baseline RMS) to find a hit
  FloatProperty m_peakThreshold{this, "PeakThreshold", 10.};
 
  //
  // Window to define fitting range, in samples (2ns/sample)
  IntegerProperty m_windowStart{this, "FitWindowStart", -10};
  IntegerProperty m_windowWidth{this, "FitWindowWidth", 50};

  //
  // Remove overflow values from CB fit
  BooleanProperty m_removeOverflow{this, "RemoveOverflow", true};

  //
  // Look for more than one hit in each channel
  BooleanProperty m_findMultipleHits{this, "FindMultipleHits", false};

  //
  // Fraction of peak to set local hit time
  FloatProperty m_timingPeakFraction{this, "TimingPeakFraction", 0.45};

  // Baseline algorithms
  WaveformBaselineData& findSimpleBaseline(const RawWaveform& wave) const;
  WaveformBaselineData& findAdvancedBaseline(const RawWaveform& wave) const;

  // Find peak in wave, return windowed region in windowed_time and windowed_wave
  // Windowed region is removed from original vectors
  // Returns true if peak found, false if not
  bool findPeak(WaveformBaselineData& baseline,
		std::vector<float>& time, std::vector<float>& wave,
		std::vector<float>& windowed_time, std::vector<float>& windowed_wave) const;

  // Get estimate from waveform data itself
  WaveformFitResult& findRawHitValues(const std::vector<float> time, 
				      const std::vector<float> wave) const;

  // Fit windowed data to Gaussian (to get initial estimate of parameters
  WaveformFitResult& fitGaussian(const WaveformFitResult& raw,
				 const std::vector<float> time, 
				 const std::vector<float> wave) const;

  // Find overflows and remove points from arrays
  bool findOverflow(const WaveformBaselineData& baseline, 
		    std::vector<float>& time, std::vector<float>& wave) const;

  // Fit windowed data to CrystalBall function
  WaveformFitResult& fitCBall(const WaveformFitResult& gfit, 
			      const std::vector<float> time, 
			      const std::vector<float> wave) const;


};

#endif // WAVERECTOOLS_WAVEFORMRECONSTRUCTIONTOOL_H
