To run on Calypso MC data (from an installation (run) directory):

% source ./setup.sh

% export VP1PLUGINPATH=/home/yourname/path/to/runfolder/lib

% vti12 -mc -noautoconf -nosortdbreplicas &lt;input HITS or EVNT file&gt;

Note that VP1PLUGINPATH can be ninja-changed by asetup, and if it does not include the Calypso installation library folder, nothing will work.  Also note that it must be an ABSOLUTE (not relative) path!

You can also give the -detdescr="FASER-01" (baseline detector), -detdescr="FASER-02" (baseline + IFT), -detdescr="FASERNU-02" (baseline + IFT + emulsion) or -detdescr="FASER-TB00" (2021 Test-beam) to specify the geometry.

You also need either -globcond="OFLCOND-FASER-01" (baseline) or -globcond="OFLCOND-FASER-02" (IFT with or without emulsion, or Test-beam) flags to specify the conditions.  

The event display has no way to determine the right values for these settings (it defaults to FASER-01 and OFLCOND-FASER-01).