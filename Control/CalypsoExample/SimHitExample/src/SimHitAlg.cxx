#include "SimHitAlg.h"

SimHitAlg::SimHitAlg(const std::string& name, ISvcLocator* pSvcLocator)
: AthHistogramAlgorithm(name, pSvcLocator) { m_hist = nullptr; }

SimHitAlg::~SimHitAlg() { }

StatusCode SimHitAlg::initialize()
{
    // initialize a histogram 
    // letter at end of TH1 indicated variable type (D double, F float etc)
    m_hist = new TH1D("eLoss", "SCT Hit Energy Loss", 100, 0, 1); //first string is root object name, second is histogram title
    m_module = new TH2D("module", "SCT Hit Module", 3, -1.5, 1.5, 4, -0.5, 3.5 );
    m_moduleSide1 = new TH2D("moduleSide1", "SCT Hit Module", 3, -1.5, 1.5, 4, -0.5, 3.5 );
    m_moduleSide2 = new TH2D("moduleSide2", "SCT Hit Module", 3, -1.5, 1.5, 4, -0.5, 3.5 );
    m_pid = new TH1D("pid", "particle_ID", 200, -100, 100); //added by Di
    m_mass = new TH1D("mass", "particle_mass", 100,0,200);
    m_px = new TH1D("px", "particle_px",10000,-100000,100000);

    ATH_CHECK(histSvc()->regHist("/HIST/eloss", m_hist));
    ATH_CHECK(histSvc()->regHist("/HIST/modules", m_module));
    ATH_CHECK(histSvc()->regHist("/HIST/modulesSide1", m_moduleSide1));
    ATH_CHECK(histSvc()->regHist("/HIST/modulesSide2", m_moduleSide2));
    ATH_CHECK(histSvc()->regHist("/HIST/pid",m_pid)); //added by Di
    ATH_CHECK(histSvc()->regHist("/HIST/mass",m_mass)); 
    ATH_CHECK(histSvc()->regHist("/HIST/px",m_px));

    m_plate_preshower = new TH2D("plate", "Scint Hit Plate", 3, -1, 1, 4, 0, 1 );
    m_plate_trigger = new TH2D("plate", "Scint Hit Plate", 3, -1, 1, 4, 0, 1 );
    m_plate_veto = new TH2D("plate", "Scint Hit Plate", 3, -1, 1, 4, 0, 1 );
    ATH_CHECK(histSvc()->regHist("/HIST/plate_preshower", m_plate_preshower));
    ATH_CHECK(histSvc()->regHist("/HIST/plate_trigger", m_plate_trigger));
    ATH_CHECK(histSvc()->regHist("/HIST/plate_veto", m_plate_veto));

    m_ecalEnergy = new TH1D("ecalEnergy", "Ecal Energy Fraction", 100, 0.0, 0.20);
    ATH_CHECK(histSvc()->regHist("/HIST/ecal_energy", m_ecalEnergy));

    // initialize data handle keys
    ATH_CHECK( m_mcEventKey.initialize() );
    ATH_CHECK( m_faserSiHitKey.initialize() );

    ATH_CHECK( m_emulsionHitKey.initialize() );

    ATH_CHECK( m_preshowerHitKey.initialize() );
    ATH_CHECK( m_triggerHitKey.initialize() );
    ATH_CHECK( m_vetoHitKey.initialize() );

    ATH_CHECK( m_ecalHitKey.initialize() );

    ATH_MSG_INFO( "Using GenEvent collection with key " << m_mcEventKey.key());
    ATH_MSG_INFO( "Using Faser SiHit collection with key " << m_faserSiHitKey.key());

    ATH_MSG_INFO( "Using Emulsion NeutrinoHit collection with key " << m_emulsionHitKey.key() );

    ATH_MSG_INFO( "Using Preshower ScintHit collection with key " << m_preshowerHitKey.key());
    ATH_MSG_INFO( "Using Trigger ScintHit collection with key " << m_triggerHitKey.key());
    ATH_MSG_INFO( "Using Veto ScintHit collection with key " << m_vetoHitKey.key());

    ATH_MSG_INFO( "Using CaloHit collection with key " << m_ecalHitKey.key());
    return StatusCode::SUCCESS;


}

StatusCode SimHitAlg::execute()
{
    // Handles created from handle keys behave like pointers to the corresponding container
    SG::ReadHandle<McEventCollection> h_mcEvents(m_mcEventKey);
    ATH_MSG_INFO("Read McEventContainer with " << h_mcEvents->size() << " events");
    if (h_mcEvents->size() == 0) return StatusCode::FAILURE;

    SG::ReadHandle<FaserSiHitCollection> h_siHits(m_faserSiHitKey);
    ATH_MSG_INFO("Read FaserSiHitCollection with " << h_siHits->size() << " hits");

    SG::ReadHandle<NeutrinoHitCollection> h_emulsionHits(m_emulsionHitKey);
    if (h_emulsionHits.isValid())
    {
        ATH_MSG_INFO("Read NeutrinoHitCollection with " << h_emulsionHits->size() << " hits");
    }

    SG::ReadHandle<ScintHitCollection> h_preshowerHits(m_preshowerHitKey);
    ATH_MSG_INFO("Read ScintHitCollection/Preshower with " << h_preshowerHits->size() << " hits");
    SG::ReadHandle<ScintHitCollection> h_triggerHits(m_triggerHitKey);
    ATH_MSG_INFO("Read ScintHitCollection/Trigger with " << h_triggerHits->size() << " hits");
    SG::ReadHandle<ScintHitCollection> h_vetoHits(m_vetoHitKey);
    ATH_MSG_INFO("Read ScintHitCollectionVeto with " << h_vetoHits->size() << " hits");
    
    SG::ReadHandle<CaloHitCollection> h_ecalHits(m_ecalHitKey);

    // Since we have no pile-up, there should always be a single GenEvent in the container
    const HepMC::GenEvent* ev = (*h_mcEvents)[0];
    if (ev == nullptr) 
    {
        ATH_MSG_FATAL("GenEvent pointer is null");
        return StatusCode::FAILURE;
    }
    ATH_MSG_INFO("Event contains " << ev->particles_size() << " truth particles" );

    double ePrimary = 0;
    if (ev->particles_size() > 0){
        HepMC::GenEvent::particle_const_iterator p = ev->particles_begin();
        ePrimary = (*p)->momentum().e();
    }


//////////// link particle test by Di ///////////////////////////////
/*
    NeutrinoHitConstIterator hitItr = h_emulsionHits->begin();
    NeutrinoHitConstIterator hitItrEnd = h_emulsionHits->end();
    for( ; hitItr != hitItrEnd ; hitItr++){
            HepMcParticleLink partLink = (*hitItr).particleLink();
            const HepMC::GenParticle * truthParticle = partLink.cptr();
    //        ATH_MSG_INFO("Link test: The particle ID is  " << truthParticle->pdg_id() << "just a test " );
            m_pid->Fill(truthParticle->pdg_id());
    }
*/
//////////// end link particle by Di ////////////////////////////////
    double pt; 
    if (h_emulsionHits.isValid() && h_emulsionHits->size()!=0)
    {
        //Loop over all hits; print
        for (const NeutrinoHit& hit : *h_emulsionHits)
        {
            hit.print();
	    if(hit.trackNumber()!=0){
		    HepMcParticleLink partLink = hit.particleLink();
		    const HepMC::GenParticle * truthParticle = partLink.cptr();
		    m_pid->Fill(truthParticle->pdg_id());
		    pt = truthParticle->momentum().rho();
		    m_px->Fill(pt/1000);
	    }
        }
    }

    // test by Di link particles


    // The hit container might be empty because particles missed the wafers
    //if (h_siHits->size() == 0) return StatusCode::SUCCESS;

    if (h_siHits->size()!=0){
        // Loop over all hits; print and fill histogram
        for (const FaserSiHit& hit : *h_siHits)
        {
            hit.print();
            m_hist->Fill( hit.energyLoss() );
            m_module->Fill( hit.getModule(), hit.getRow() );
            if (hit.getSensor() == 0)
            {
                m_moduleSide1->Fill( hit.getModule(), hit.getRow());
            }
            else
            {
                m_moduleSide2->Fill( hit.getModule(), hit.getRow());
            }

        }
    }

    if (h_preshowerHits->size()!=0){
        for (const ScintHit& hit : *h_preshowerHits)
        {
            hit.print();
            m_hist->Fill(hit.energyLoss());
            m_plate_preshower->Fill(hit.getStation(),hit.getPlate(),hit.energyLoss());

        }
    }

    if (h_triggerHits->size()!=0){
        for (const ScintHit& hit : *h_triggerHits)
        {
            hit.print();
            m_hist->Fill(hit.energyLoss());
            m_plate_trigger->Fill(hit.getStation(),hit.getPlate(),hit.energyLoss());

        }
    }

    if (h_vetoHits->size()!=0){
        for (const ScintHit& hit : *h_vetoHits)
        {
            hit.print();
            m_hist->Fill(hit.energyLoss());
            m_plate_veto->Fill(hit.getStation(),hit.getPlate(),hit.energyLoss());

        }
    }

    if (h_ecalHits->size() != 0)
    {
        double ecalTotal = 0.0;
        for (const CaloHit& hit : *h_ecalHits)
        {
            ecalTotal += hit.energyLoss();
        }
        if (ePrimary > 0) m_ecalEnergy->Fill(ecalTotal/ePrimary);
    }
    return StatusCode::SUCCESS;
}

StatusCode SimHitAlg::finalize()
{
    return StatusCode::SUCCESS;
}
