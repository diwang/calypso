#!/usr/bin/env python
#
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
# Run with:
# ./faser_reco.py filepath [runtype]
# 
# filepath - fully qualified path, including url if needed, to the input raw data file
#   example: "root://hepatl30//atlas/local/torrence/faser/commissioning/TestBeamData/Run-004150/Faser-Physics-004150-00000.raw"
# 
# runtype - optional flag to specify the data type (TI12Data or TestBeamData).
#   In a normal file system location, this will be extracted from the directory name,
#   but runtype will override this assignment. 
#
import sys
import argparse

parser = argparse.ArgumentParser(description="Run FASER reconstruction")

parser.add_argument("file_path",
                    help="Fully qualified path of the raw input file")
parser.add_argument("run_type", nargs="?", default="",
                    help="Specify run type (if it can't be parsed from path)")
parser.add_argument("-r", "--reco", default="",
                    help="Specify reco tag (to append to output filename)")
parser.add_argument("-n", "--nevents", type=int, default=-1,
                    help="Specify number of events to process (default: all)")

args = parser.parse_args()

from pathlib import Path

filepath=Path(args.file_path)

# runtype has been provided
if len(args.run_type) > 0:
    runtype=args.run_type

# Extract runtype from path
# Should be directory above run
# i.e.: TestBeamData/Run-004150/Faser-Physics-004150-00000.raw"
else:
    if len(filepath.parts) < 3:
        print("Can't determine run type from path - specify on command line instead")
        sys.exit(-1)

    runtype = filepath.parts[-3]

print(f"Starting reconstruction of {filepath.name} with type {runtype}")
if args.nevents > 0:
    print(f"Reconstructing {args.nevents} events by command-line option")

# Start reconstruction

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaCommon.Constants import VERBOSE, INFO

from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags

Configurable.configurableRun3Behavior = True
    
# Flags for this job
ConfigFlags.Input.isMC = False                               # Needed to bypass autoconfig
ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"               # Use MC conditions for now

ConfigFlags.Input.ProjectName = "data20"
ConfigFlags.GeoModel.Align.Dynamic    = False

# TI12 Cosmics geometry
if runtype == "TI12Data":
    ConfigFlags.GeoModel.FaserVersion = "FASER-01" 
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"

# Testbeam setup 
elif runtype == "TestBeamData":
    ConfigFlags.GeoModel.FaserVersion = "FASER-TB00" 
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-02"

else:
    print("Invalid run type found:", runtype)
    print("Specify correct type or update list")
    sys.exit(-1)


# Must use original input string here, as pathlib mangles double // in path names
ConfigFlags.Input.Files = [ args.file_path ]

filestem = filepath.stem
if len(args.reco) > 0:
    filestem += f"-{args.reco}"

ConfigFlags.addFlag("Output.xAODFileName", f"{filestem}-xAOD.root")
ConfigFlags.Output.ESDFileName = f"{filestem}-ESD.root"

#
# Play around with this?
# ConfigFlags.Concurrency.NumThreads = 2
# ConfigFlags.Concurrency.NumConcurrentEvents = 2
ConfigFlags.lock()

#
# Configure components
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
    
acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolWriteCfg(ConfigFlags))

#
# Set up RAW data access
from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg
acc.merge(FaserByteStreamCnvSvcCfg(ConfigFlags))

#
# Needed, or move to MainServicesCfg?
from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
acc.merge(FaserGeometryCfg(ConfigFlags))

# Set up algorithms
from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionCfg    
acc.merge(WaveformReconstructionCfg(ConfigFlags))

# Tracker clusters
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))

# ... try SpacePoints
#from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
#acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))

# Try Dave's fitter
from TrackerClusterFit.TrackerClusterFitConfig import ClusterFitAlgCfg
acc.merge(ClusterFitAlgCfg(ConfigFlags))

#
# Configure output
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
itemList = [ "xAOD::EventInfo#*"
             , "xAOD::EventAuxInfo#*"
             , "xAOD::FaserTriggerData#*"
             , "xAOD::FaserTriggerDataAux#*"
             , "FaserSCT_RDO_Container#*"
             , "Tracker::FaserSCT_ClusterContainer#*"
             #, "Tracker::SCT_SpacePointContainer#*"
             #, "Tracker::SCT_SpacePointOverlapCollection#*"
             , "TrackCollection#*"
]
acc.merge(OutputStreamCfg(ConfigFlags, "xAOD", itemList))

# Waveform reconstruction
from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionOutputCfg    
acc.merge(WaveformReconstructionOutputCfg(ConfigFlags))

# Check what we have
print( "Writing out xAOD objects:" )
print( acc.getEventAlgo("OutputStreamxAOD").ItemList )

# Configure verbosity    
# ConfigFlags.dump()
# logging.getLogger('forcomps').setLevel(VERBOSE)
# acc.foreach_component("*").OutputLevel = VERBOSE
acc.foreach_component("*").OutputLevel = INFO
acc.foreach_component("*ClassID*").OutputLevel = INFO
# log.setLevel(VERBOSE)

#acc.getService("FaserByteStreamInputSvc").DumpFlag = True
#acc.getService("FaserEventSelector").OutputLevel = VERBOSE
#acc.getService("FaserByteStreamInputSvc").OutputLevel = VERBOSE
#acc.getService("FaserByteStreamCnvSvc").OutputLevel = VERBOSE
#acc.getService("FaserByteStreamAddressProviderSvc").OutputLevel = VERBOSE
acc.getService("MessageSvc").Format = "% F%40W%S%7W%R%T %0W%M"

# Execute and finish
sys.exit(int(acc.run(maxEvents=args.nevents).isFailure()))
