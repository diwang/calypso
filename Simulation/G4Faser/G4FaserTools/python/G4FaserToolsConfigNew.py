# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
from __future__ import print_function
from AthenaConfiguration.ComponentFactory import CompFactory


from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaCommon import CfgMgr
SensitiveDetectorMasterTool=CompFactory.SensitiveDetectorMasterTool
FastSimulationMasterTool=CompFactory.FastSimulationMasterTool

def generateFastSimulationListCfg(ConfigFlags):
    result = ComponentAccumulator()
    FastSimulationList = []
    result.setPrivateTools(FastSimulationList)
    return result

def FastSimulationMasterToolCfg(ConfigFlags, **kwargs):
    result = ComponentAccumulator()
    FastSimulationList = result.popToolsAndMerge(generateFastSimulationListCfg(ConfigFlags))
    kwargs.setdefault("FastSimulations", FastSimulationList)
    result.setPrivateTools(FastSimulationMasterTool(name="FastSimulationMasterTool", **kwargs))
    return result

from EmulsionG4_SD.EmulsionG4_SDToolConfig import EmulsionSensorSDCfg
from VetoG4_SD.VetoG4_SDToolConfig import VetoSensorSDCfg
from TriggerG4_SD.TriggerG4_SDToolConfig import TriggerSensorSDCfg
from PreshowerG4_SD.PreshowerG4_SDToolConfig import PreshowerSensorSDCfg
from FaserSCT_G4_SD.FaserSCT_G4_SDToolConfig import SctSensorSDCfg
from EcalG4_SD.EcalG4_SDToolConfig import EcalSensorSDCfg

def generateNeutrinoSensitiveDetectorList(ConfigFlags):

    result = ComponentAccumulator()
    SensitiveDetectorList=[]

    if ConfigFlags.Detector.EnableEmulsion:
        accEmulsion,toolEmulsion = EmulsionSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolEmulsion ]
        result.merge(accEmulsion)
    
    return result, SensitiveDetectorList #List of tools here now! (CALL IT TOOL LIST?)

def generateScintSensitiveDetectorList(ConfigFlags):

    result = ComponentAccumulator()
    SensitiveDetectorList=[]

    if ConfigFlags.Detector.EnableVeto:
        accVeto,toolVeto = VetoSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolVeto ]
        result.merge(accVeto)
    
    if ConfigFlags.Detector.EnableTrigger:
        accTrigger,toolTrigger = TriggerSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolTrigger ]
        result.merge(accTrigger)
    
    if ConfigFlags.Detector.EnablePreshower:
        accPreshower,toolPreshower = PreshowerSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolPreshower ]
        result.merge(accPreshower)
    
    return result, SensitiveDetectorList #List of tools here now! (CALL IT TOOL LIST?)

def generateTrackerSensitiveDetectorList(ConfigFlags):

    result = ComponentAccumulator()
    SensitiveDetectorList=[]

    if ConfigFlags.Detector.EnableFaserSCT:
        accSCT,toolSCT = SctSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolSCT ]
        result.merge(accSCT)
        
    return result, SensitiveDetectorList #List of tools here now! (CALL IT TOOL LIST?)

def generateCaloSensitiveDetectorList(ConfigFlags):

    result = ComponentAccumulator()
    SensitiveDetectorList=[]

    if ConfigFlags.Detector.EnableEcal:
        accEcal,toolEcal = EcalSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolEcal ]
        result.merge(accEcal)
    
    return result, SensitiveDetectorList #List of tools here now! (CALL IT TOOL LIST?)

def generateSensitiveDetectorList(ConfigFlags):
    result = ComponentAccumulator()
    SensitiveDetectorList=[]
    # SensitiveDetectorList += generateEnvelopeSensitiveDetectorList(ConfigFlags) # to update

    acc_NeutrinoSensitiveDetector, NeutrinoSensitiveDetectorList = generateNeutrinoSensitiveDetectorList(ConfigFlags)
    SensitiveDetectorList += NeutrinoSensitiveDetectorList

    acc_ScintSensitiveDetector, ScintSensitiveDetectorList = generateScintSensitiveDetectorList(ConfigFlags)
    SensitiveDetectorList += ScintSensitiveDetectorList

    acc_TrackerSensitiveDetector, TrackerSensitiveDetectorList = generateTrackerSensitiveDetectorList(ConfigFlags)
    SensitiveDetectorList += TrackerSensitiveDetectorList

    acc_CaloSensitiveDetector, CaloSensitiveDetectorList = generateCaloSensitiveDetectorList(ConfigFlags)
    SensitiveDetectorList += CaloSensitiveDetectorList

    result.merge(acc_NeutrinoSensitiveDetector)
    result.merge(acc_ScintSensitiveDetector)
    result.merge(acc_TrackerSensitiveDetector)
    result.merge(acc_CaloSensitiveDetector)

    result.setPrivateTools(SensitiveDetectorList)
    return result

def SensitiveDetectorMasterToolCfg(ConfigFlags, name="SensitiveDetectorMasterTool", **kwargs):
    result = ComponentAccumulator()
    accSensitiveDetector = generateSensitiveDetectorList(ConfigFlags)
    kwargs.setdefault("SensitiveDetectors", result.popToolsAndMerge(accSensitiveDetector)) #list of tools

    result.setPrivateTools(SensitiveDetectorMasterTool(name, **kwargs)) #note -this is still a public tool
    return result

def getEmptySensitiveDetectorMasterTool(name="EmptySensitiveDetectorMasterTool", **kwargs):
    return CfgMgr.SensitiveDetectorMasterTool(name, **kwargs)
