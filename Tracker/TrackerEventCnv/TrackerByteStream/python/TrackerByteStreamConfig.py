# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def TrackerByteStreamCfg(configFlags, **kwargs):
    acc = ComponentAccumulator()

    return acc