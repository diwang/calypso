/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

#include "SpacePointContainerCnv.h"

SpacePointContainer_PERS*
SpacePointContainerCnv::createPersistent (SpacePointContainer* transObj) {
  ATH_MSG_INFO("SpacePointContainerCnv::createPersistent()");

  SpacePointContainerCnv_PERS converter;

  SpacePointContainer_PERS* persObj(nullptr);
  persObj = converter.createPersistent(transObj, msg());
  return persObj;
}


SpacePointContainer*
SpacePointContainerCnv::createTransient() {
  ATH_MSG_INFO("SpacePointContainerCnv::createTransient()");

  static const pool::Guid p0_guid("DB0397F9-A163-496F-BC17-C7E507A1FA50");
  SpacePointContainer* transObj(nullptr);

  if (compareClassGuid(p0_guid)) {
    std::unique_ptr<SpacePointContainer_PERS> col_vect(poolReadObject<SpacePointContainer_PERS>());
    SpacePointContainerCnv_PERS converter;
    transObj = converter.createTransient( col_vect.get(), msg());
  } else {
    throw std::runtime_error("Unsupported persistent version of SpacePointContainer");
  }

  return transObj;
}
