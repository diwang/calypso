/*
 Copyright 2021 CERN for the benefit of the FASER collaboration
*/

#ifndef SPACEPOINTCONTAINERCNV_H
#define SPACEPOINTCONTAINERCNV_H

#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"

#include "TrackerEventTPCnv/SpacePointContainerCnv_p0.h"

#include "TrkSpacePoint/SpacePointContainer.h"
#include "TrackerEventTPCnv/SpacePointContainer_p0.h"

typedef SpacePointContainer_p0 SpacePointContainer_PERS;
typedef SpacePointContainerCnv_p0 SpacePointContainerCnv_PERS;

typedef T_AthenaPoolCustomCnv<SpacePointContainer, SpacePointContainer_PERS> SpacePointContainerCnvBase;

class SpacePointContainerCnv : public SpacePointContainerCnvBase {
  friend class CnvFactory<SpacePointContainerCnv>;

 public:
  SpacePointContainerCnv (ISvcLocator* svcloc) : SpacePointContainerCnvBase(svcloc) {}

 protected:
  virtual SpacePointContainer_PERS* createPersistent (SpacePointContainer* transObj);
  virtual SpacePointContainer* createTransient ();
};

#endif  // SPACEPOINTCONTAINERCNV_H
